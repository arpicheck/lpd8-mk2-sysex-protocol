# LPD8 mk2 sysex protocol

Basically the controller accepts requests over SysEx.
To issue a request, send the following sysex for your AKAI PLD8 mk2:




## Request program data over sysex

*Sysex start (offset 0, size 1)*
- [ ] **f0**

*Header (offset 1, size, 7)*
- [ ] **47 7f 4c 03 00 01**
- [ ] **01** - program number

*Sysex end (offset 8, size 1)*
- [ ] **f7**



## Program data sent by the controller (173 bytes)

*Sysex start (offset 0, size 1)*
- [ ] **f0**

*Header (offest 1, size 7)*
- [ ] **47 7f 4c 03 01 29**
- [ ] **01** - program number

*Global parameters (offset 8, size 4)*
- [ ] **00** - type
- [ ] **02** - pressure message
- [ ] **01** - full level
- [ ] **00** - channel


*Pads (offset 12, size 8x16)*
- [ ] **24** - note
- [ ] **0c** - cc
- [ ] **00** - program
- [ ] **09** - channel (-1)
- [ ] **0000 0100 0000** - off color 
- [ ] **0100 0100 0100** - on color

> **About colors**
> 
> One byte is represented on two, because of sysex, which does not allow 8 bit bytes
> 
>#808040 will end up in 6 bytes 
> 
> **0100 0100 0040**

*7 more pads*

>250d0109000001000000010001000100
260e0209000001000000010001000100
270f0309000001000000010001000100
28100409000001000000010001000100
29110509000001000000010001000100
2a120609000001000000010001000100
2b130709000001000000010001000100

*Knobs (offset 140, size 32)*
- [ ] **46** - cc
- [ ] **10** - channel, 16 stands for "global"
- [ ] **00** - min value
- [ ] **7f** - max value

*7 more knobs*

>4710007f
4810007f
4910007f
4a10007f
4b10007f
4c10007f
4d10007f

*Sysex end (offset 172, size 1)*
- [ ] **f7**